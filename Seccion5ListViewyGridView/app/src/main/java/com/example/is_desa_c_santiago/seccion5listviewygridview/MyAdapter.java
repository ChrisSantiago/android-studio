package com.example.is_desa_c_santiago.seccion5listviewygridview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class MyAdapter extends BaseAdapter {

    private Context context;
    private int layout;
    private List<String> names;

    public MyAdapter(Context context, int layout, List<String> names) {
        this.context = context;
        this.layout = layout;
        this.names = names;
    }

    @Override
    public int getCount() {
        return this.names.size();
    }

    @Override
    public Object getItem(int position) {
        return names.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //View holder pattern
        ViewHolder holder;

        //Se valida si la vista de los items es nula
        if(convertView == null){
            //Tomamos del contexto el layoutImflate
            LayoutInflater layoutInflater = LayoutInflater.from(this.context);
            //Inflamos el layout, lo mejoramos
            convertView = layoutInflater.inflate(this.layout, null);

            holder = new ViewHolder();
            //Referenciamos el objecto a modificar y la reciclamos
            holder.nuevoTextView = (TextView) convertView.findViewById(R.id.textView);

            //Metemos el objecto sin id
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        //Recogemos el view que llega
        //-- View view = convertView;

        //Tomamos del contexto el layoutImflate
        //-- LayoutInflater layoutInflater = LayoutInflater.from(this.context);

        //Inflamos el layout, lo mejoramos
        //-- view = layoutInflater.inflate(R.layout.list_item, null);

        String currentName = names.get(position);
//        currentName = (String) this.getItem(position);

        //Referenciamos el objecto a modificar y la reciclamos
        //-- TextView textView = (TextView) view.findViewById(R.id.textView);
        //-- textView.setText(currentName);
        holder.nuevoTextView.setText(currentName);
        // Devolvemos ia vista modificada e inflada
        return convertView;
    }

    static class ViewHolder{
        private TextView nuevoTextView;
    }
}


