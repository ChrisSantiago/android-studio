package com.example.is_desa_c_santiago.seccion5listviewygridview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;



import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private List<String> names;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.listView = (ListView)findViewById(R.id.listView);

        //Datos a mostar
         names = new ArrayList<String>(){{
            add("Alejandro");
            add("Sonia");
            add("Jhonatan");
            add("Christofer");
            add("Muñeca");
            add("Canela");
            add("Alejandro");
            add("Sonia");
            add("Jhonatan");
            add("Christofer");
            add("Muñeca");
            add("Canela");

        }};

        //Adaptado la forma visual que mostraremos los datos
        // para rederizar (comunicar) lista, arreglos en views
        //-- ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, names);

        //Establesemos el adaptador con nuestro listview
        //-- listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this,"Clikeado en " + names.get(position), Toast.LENGTH_SHORT).show();
            }
        });

        //Enlazamos nuestro adaptado personalizado
        MyAdapter myAdapter = new MyAdapter(MainActivity.this,R.layout.list_item,names);
        listView.setAdapter(myAdapter);
    }
}

