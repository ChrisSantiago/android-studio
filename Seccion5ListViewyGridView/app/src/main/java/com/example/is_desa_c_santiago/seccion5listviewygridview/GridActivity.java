package com.example.is_desa_c_santiago.seccion5listviewygridview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class GridActivity extends AppCompatActivity {

    private List<String> names;
    private GridView gridView;
    private int contador = 0;
    private MyAdapter myAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid);

        this.gridView = (GridView) findViewById(R.id.gridView);
        //Datos a mostar
        names = new ArrayList<String>() {{
            add("Alejandro");
            add("Sonia");
            add("Jhonatan");
            add("Christofer");
            add("Muñeca");
            add("Canela");

        }};

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(GridActivity.this, "Clikeado en " + names.get(position), Toast.LENGTH_SHORT).show();
            }
        });

        //Enlazamos nuestro adaptado personalizado
        myAdapter = new MyAdapter(GridActivity.this, R.layout.grid_layout, names);
        gridView.setAdapter(myAdapter);

        registerForContextMenu(gridView);
    }


    //Inflamos vista de menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.action_bar_menu, menu);

        return true;
    }

    //Manejamos eventos de menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addItem:
                //Añadimos un nuevo nombre
                this.names.add("Added n: " + (++contador));
                // Notificamos al adaptador que hemos agregado un nuevo item
                this.myAdapter.notifyDataSetChanged();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    //Inflamos la vista del context menu
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater menuInflater = getMenuInflater();
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        menu.setHeaderTitle(this.names.get(info.position));
        menuInflater.inflate(R.menu.context_menu, menu);
    }

    //Manejamos eventos del context menu
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        //Traemos la informacion del item oprimido
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.deleteItem:
                //eliminamos item
                this.names.remove(info.position);
                myAdapter.notifyDataSetChanged();
                return true;
            default:
                return super.onContextItemSelected(item);

        }

    }
}
