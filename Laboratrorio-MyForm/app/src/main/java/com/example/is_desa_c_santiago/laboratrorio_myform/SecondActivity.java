package com.example.is_desa_c_santiago.laboratrorio_myform;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {
    private RadioButton radioButtonHello;
    private RadioButton radioButtonGoodbye;
    private SeekBar seekBarAge;
    private Button buttonGoTo;
    private TextView textViewAge;
    private int age = 0;;
    private final int MIN_AGE = 16;
    public static final int HELLO = 1;
    public static final int GOODBYE = 2;
    private String name = "";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.radioButtonHello = (RadioButton)findViewById(R.id.radioButtonHello);
        this.radioButtonGoodbye = (RadioButton)findViewById(R.id.radioButtonGoodbye);
        this.seekBarAge = (SeekBar)findViewById(R.id.seekBarAge);
        this.buttonGoTo = (Button)findViewById(R.id.buttonGoTo);
        this.textViewAge = (TextView)findViewById(R.id.textViewAge);

        this.buttonGoTo.setVisibility(View.INVISIBLE);
        radioButtonHello.setChecked(true);
        this.seekBarAge.setProgress(18);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            name = bundle.getString("name");
        }

        this.seekBarAge.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                age = progress;
                textViewAge.setText(String.valueOf(age) + " "+ getString(R.string.age));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                age = seekBarAge.getProgress();
                textViewAge.setText(String.valueOf(age) + " " + getString(R.string.age));
                if(age > MIN_AGE){
                    buttonGoTo.setVisibility(View.VISIBLE);
                } else {
                    buttonGoTo.setVisibility(View.INVISIBLE);
                    Toast.makeText(SecondActivity.this, "I'm sorry you be older that 16 years old",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        this.buttonGoTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SecondActivity.this, ThirdActivity.class);
                intent.putExtra("name", name);
                int option = (radioButtonGoodbye.isChecked() ? GOODBYE : HELLO);
                intent.putExtra("acction", option);
                intent.putExtra("age", age);
                startActivity(intent);
            }
        });

    }


}
