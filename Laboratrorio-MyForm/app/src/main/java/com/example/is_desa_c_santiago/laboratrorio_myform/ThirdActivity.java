package com.example.is_desa_c_santiago.laboratrorio_myform;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class ThirdActivity extends AppCompatActivity {

    private Button buttonAction;
    private ImageButton imageButtonShare;
    private String name = "";
    private int age = 0;
    private int option = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.buttonAction = (Button) findViewById(R.id.buttonAction);
        this.imageButtonShare = (ImageButton)findViewById(R.id.imageButtonShare);

        imageButtonShare.setVisibility(View.INVISIBLE);
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            name = bundle.getString("name");
            age = bundle.getInt("age");
            option = bundle.getInt("acction");
        }

        this.buttonAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonAction.setVisibility(View.INVISIBLE);
                imageButtonShare.setVisibility(View.VISIBLE);
                Toast.makeText(ThirdActivity.this, getMessage(name, age, option), Toast.LENGTH_LONG).show();

            }
        });

        this.imageButtonShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, getMessage(name, age, option));
                startActivity(intent);
            }
        });
    }

    public String getMessage(String name, int age, int option){
        if(option == SecondActivity.HELLO){
            return ("Hi " + name +"!, ¿What about this " + age + " years");
        } else {
            return ("I hope to see you soon " + name + ", before you be " +  (age + 1) + " years old");
        }
    }
}
