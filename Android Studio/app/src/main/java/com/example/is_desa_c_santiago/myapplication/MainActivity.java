package com.example.is_desa_c_santiago.myapplication;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Magnifier;
import android.widget.Toast;

import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {
    private EditText editTextPhone;
    private EditText editTextWeb;
    private ImageButton imageButtonPhone;
    private ImageButton imageButtonWeb;
    private ImageButton imageButtonMail;
    private ImageButton imageButtonCamara;
    private EditText editTextMail;
    private final int PHONE_CALL_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.editTextPhone = (EditText) findViewById(R.id.editTextPhone);
        this.editTextWeb = (EditText) findViewById(R.id.editTextWeb);
        this.imageButtonWeb = (ImageButton) findViewById(R.id.imageButtonWeb);
        this.imageButtonPhone = (ImageButton) findViewById(R.id.imageButtonPhone);
        this.editTextMail = (EditText) findViewById(R.id.editTextMail);
        this.imageButtonMail = (ImageButton) findViewById(R.id.imageButtonMail);
        this.imageButtonCamara = (ImageButton) findViewById(R.id.imageButtonCamara);

        //Forzar y cargar  el icono de actionbar

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_eiffel);

        imageButtonPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = editTextPhone.getText().toString();
                if (phoneNumber != null && !phoneNumber.isEmpty()){
                    //Comprobar version de telefono
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        //Comprobar si ha aceptado, no ha aceptado, o nunca se le ha preguntado
                        if (checkPermission(Manifest.permission.CALL_PHONE)){
                            Intent intentCall = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
                            startActivity(intentCall);
                        } else {
                            //No ha aceptado o es la primera vez
                            if(!shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)){
                                // No se ha preguntado aun
                                requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PHONE_CALL_CODE);
                            } else {
                                Toast.makeText(MainActivity.this,"Por vafor, debe de aceptar los permisos de callamada",
                                        Toast.LENGTH_SHORT).show();
                                Intent intentTools = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                intentTools.addCategory(Intent.CATEGORY_DEFAULT);
                                intentTools.setData(Uri.parse("package:" + getPackageName()));
                                intentTools.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intentTools.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                intentTools.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                                startActivity(intentTools);
                            }
                        }
                    } else {
                        olderVersions(phoneNumber);
                    }
                }else {
                    Toast.makeText(MainActivity.this,"Upps! no voy a llamar si no me das un número de telefono valido", Toast.LENGTH_SHORT).show();
                }
            }

            public void olderVersions(String phoneNumber){
                Intent intentCall = new Intent(Intent.ACTION_CALL,Uri.parse("tel:" + phoneNumber));
                if (checkPermission(Manifest.permission.CALL_PHONE)){
                    startActivity(intentCall);
                } else {
                    Toast.makeText(MainActivity.this,"Usted no tiene permisos para llamar", Toast.LENGTH_SHORT).show();
                }

            }
        });

        //Intent web
        imageButtonWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchWeb = editTextWeb.getText().toString();
                String mail = "chriscoke66@hotmail.com";
                if (searchWeb != null && !searchWeb.isEmpty()){
                    Intent intentWeb = new Intent();
                    intentWeb.setAction(Intent.ACTION_VIEW);
                    intentWeb.setData(Uri.parse("http://"+searchWeb));

                    //Intent de contactos
                    Intent intentContacts = new Intent(Intent.ACTION_VIEW, Uri.parse("content://contacts/people"));

                    //Intent email simple
                    Intent intentMailTo = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + mail));

                    //email completo
                    Intent intentMail = new Intent(Intent.ACTION_VIEW, Uri.parse(mail));
                    intentMail.setType("message/rfc822");
                    intentMail.putExtra(Intent.EXTRA_SUBJECT,"MY TRAVEL TIO PARIS");
                    intentMail.putExtra(Intent.EXTRA_TEXT,"Hi I am very happy for my next travel");
                    intentMail.putExtra(Intent.EXTRA_EMAIL, new String[] {"chrissantiago04@gmail.com", "jhonale44@hotmail.com"});
                    //startActivity(Intent.createChooser(intentMail,"¿Cual es tú cliente de correos favorito?"));
                    //Intent llamada 2
                    Intent intentPhoneTwo = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:66611111"));
                    startActivity(intentWeb);
                } else {
                    Toast.makeText(MainActivity.this,"Upps! no búescare si no hay un sitio web", Toast.LENGTH_SHORT).show();
                }
            }
        });


        imageButtonMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mail = editTextMail.getText().toString();
                if (mail != null && !mail.isEmpty()){
                    if (Pattern.matches("^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\\.([a-zA-Z]{2,4})+$", mail)){
                        Intent intentMail = new Intent(Intent.ACTION_SEND, Uri.parse(mail));
                        intentMail.setType("plain/text");
                        intentMail.putExtra(Intent.EXTRA_EMAIL, new String[]{"jhonale44@hotmail.com"});
                        intentMail.putExtra(Intent.EXTRA_SUBJECT, "TRAVEL TO PARIS");
                        intentMail.putExtra(Intent.EXTRA_TEXT,"I can't wait for my travel");
                        startActivity(Intent.createChooser(intentMail,"¿Cual es tú cliente de correos favorito?"));
                    } else {
                        Toast.makeText(MainActivity.this, "Tu correo está mal, lo siento", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Upps! el correo está en blanco", Toast.LENGTH_SHORT).show();
                }
            }
        });


        imageButtonCamara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentCamara = new Intent("android.media.action.IMAGE_CAPTURE");
                startActivity(intentCamara);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PHONE_CALL_CODE:
                String permission = permissions[0];
                int result = grantResults[0];
                if (permission.equals(Manifest.permission.CALL_PHONE)){
                    //Comprobar si han sido aceptado o denegado
                    if (result == PackageManager.PERMISSION_GRANTED){
                        //Consedio permiso
                        String phoneNumber = editTextPhone.getText().toString();
                        Intent intentCall = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
                        startActivity(intentCall);
                    } else {
                        //No consedio permiso
                        Toast.makeText(MainActivity.this,"Usted no tiene permisos para llamar", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
                default:
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                    break;
        }

    }

    private boolean checkPermission(String permission){
        int result = this.checkCallingOrSelfPermission(permission);
        return result == PackageManager.PERMISSION_GRANTED;
    }
}
