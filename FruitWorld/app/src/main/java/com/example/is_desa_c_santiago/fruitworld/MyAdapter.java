package com.example.is_desa_c_santiago.fruitworld;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.is_desa_c_santiago.fruitworld.Models.Fruit;

import java.util.List;

public class MyAdapter extends BaseAdapter {
    private List<Fruit> fruits;
    private int layout;
    private Context context;

    public MyAdapter(List<Fruit> fruits, int layout, Context context) {
        this.fruits = fruits;
        this.layout = layout;
        this.context = context;
    }

    @Override
    public int getCount() {
        return this.fruits.size();
    }

    @Override
    public Object getItem(int position) {
        return this.fruits.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            //Tomamos el contexto y lo pasamos para despues inflar la vista
            LayoutInflater layoutInflater = LayoutInflater.from(this.context);

            //inflamos la vista
            convertView = layoutInflater.inflate(this.layout,null);

            viewHolder = new ViewHolder();

            viewHolder.textViewName = (TextView)convertView.findViewById(R.id.textViewName);
            viewHolder.textViewOrigin = (TextView)convertView.findViewById(R.id.textViewOrigin);
            viewHolder.imageViewPicture = (ImageView)convertView.findViewById(R.id.imageViewPicture);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        Fruit fruit = fruits.get(position);
        viewHolder.textViewName.setText(fruit.getName());
        viewHolder.textViewOrigin.setText(fruit.getOrigin());
        viewHolder.imageViewPicture.setImageResource(fruit.getIcon());

        return convertView;
    }

    static class ViewHolder{
        private TextView textViewName;
        private TextView textViewOrigin;
        private ImageView imageViewPicture;
    }
}
