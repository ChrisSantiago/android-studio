package com.example.is_desa_c_santiago.fruitworld;

import android.support.v4.app.ListFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.is_desa_c_santiago.fruitworld.Models.Fruit;

import java.util.ArrayList;
import java.util.List;

public class FruitWorldActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private ListView listViewFruit;
    private List<Fruit> fruits;
    private GridView gridViewFruit;
    private MyAdapter adapterListView;
    private MyAdapter adapterGridView;

    //opciones de menu
    private MenuItem menuItemListView;
    private MenuItem menuItemGridView;

    private int contador = 0;
    private static final int OPTION_GRID_VIEW = 1;
    private static final int OPTION_LIST_VIEW = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fruit_world);

        //Mostrar el icono
        enforceIconBar();
        //Instaciamos los componentes
        listViewFruit = (ListView)findViewById(R.id.listViewFruit);
        gridViewFruit = (GridView)findViewById(R.id.gridViewFruit);
        this.fruits = getAllFruits();

        //Instaciamos los adaptaores
        adapterListView = new MyAdapter(fruits,R.layout.item_fruits, FruitWorldActivity.this);
        listViewFruit.setAdapter(adapterListView);

        adapterGridView = new MyAdapter(fruits, R.layout.item_fruit_grid, FruitWorldActivity.this);
        gridViewFruit.setAdapter(adapterGridView);


        //Adjutamos el mismo metodo clickListenner
        listViewFruit.setOnItemClickListener(this);
        gridViewFruit.setOnItemClickListener(this);

        gridViewFruit.setVisibility(View.INVISIBLE);

        //Registramos el context menu para ambos
        registerForContextMenu(listViewFruit);
        registerForContextMenu(gridViewFruit);

    }

    //Metodo para mostar el icono cuando se inicia la app
    private void enforceIconBar(){
        getSupportActionBar().setIcon(R.mipmap.ic_icon_layer);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    //ClickListenner para seleccionar un item ya se en listView y gridView
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        this.selectedFruit(this.fruits.get(position));
    }

    private void selectedFruit(Fruit fruit){
        if("Unknow".equals(fruit.getOrigin())){
            Toast.makeText(FruitWorldActivity.this, "No tenemos mucha informacion de esta fruta", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(FruitWorldActivity.this, fruit.getName() + " su origin es " + fruit.getOrigin(),
                    Toast.LENGTH_SHORT).show();
        }
    }

    //Menu sobrescrito para el MENU y OPCIONES
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater  = getMenuInflater();
        menuInflater.inflate(R.menu.action_bar_menu, menu);

        //recogemos las referencias de ls botones
        this.menuItemListView = menu.findItem(R.id.itemListView);
        this.menuItemGridView = menu.findItem(R.id.itemGridView);
        this.menuItemListView.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itemAdd:
                this.addFruit(++contador);
                return true;
            case R.id.itemListView:
                this.swicthMenu(OPTION_LIST_VIEW);
                return true;
            case R.id.itemGridView:
                this.swicthMenu(OPTION_GRID_VIEW);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //SOBRECARGA DE METODO, para realizar una accion para cuando se de un LongClickItem
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater menuInflater = getMenuInflater();
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        menu.setHeaderTitle("Delete " + fruits.get(info.position).getName());
        menuInflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.itemDelete:
                deleteFruit(info.position);
                return true;
            default:
                return super.onContextItemSelected(item);
        }

    }

    // ------------- SECCION PARA ADMINISTRAR LOS REGISTRO CRUD --------------
    public List<Fruit> getAllFruits(){
        List<Fruit> list = new ArrayList<Fruit>(){{
            add(new Fruit("Orange","Origin: Sevilla",R.mipmap.ic_orange_layer));
            add(new Fruit("Strawerry","Origin: Huelva",R.mipmap.ic_strawerry_layer));
            add(new Fruit("Pear","Origin: Zaragoza",R.mipmap.ic_pear_layer));
            add(new Fruit("Apple","Origin: Madrid",R.mipmap.ic_apple_layer));
            add(new Fruit("Cherry","Origin: Galicia",R.mipmap.ic_cherry_layer));
            add(new Fruit("Raspberry","Origin: Barcelona",R.mipmap.ic_raspberry_layer));
            add(new Fruit("Banana","Origen: Gran Canaria", R.mipmap.ic_banana_layer));
        }};

        return list;
    }

    private void addFruit(int contador){
        Fruit fruit = new Fruit("Fruit" + contador,"Unknow",R.mipmap.ic_new_fruit_layer);
        this.fruits.add(fruit);
        adapterListView.notifyDataSetChanged();
        adapterGridView.notifyDataSetChanged();
    }

    private void deleteFruit(int position){
        this.fruits.remove(position);
        adapterGridView.notifyDataSetChanged();
        adapterListView.notifyDataSetChanged();
    }

    //Metodo para cambiar las vista por listView y gridView
    private void swicthMenu(int option){
        if(this.OPTION_LIST_VIEW == option){
            if(this.listViewFruit.getVisibility() == View.INVISIBLE){
                //avilitamos la opcion de menu y listview
                this.menuItemGridView.setVisible(true);
                this.listViewFruit.setVisibility(View.VISIBLE);

                //desailitamos la opcion y gridview
                this.menuItemListView.setVisible(false);
                this.gridViewFruit.setVisibility(View.INVISIBLE);
            }


        } else if(this.OPTION_GRID_VIEW == option){
            if(this.gridViewFruit.getVisibility() == View.INVISIBLE){
                //ABILITAMOS VISTAS
                this.menuItemListView.setVisible(true);
                this.gridViewFruit.setVisibility(View.VISIBLE);

                this.listViewFruit.setVisibility(View.INVISIBLE);
                this.menuItemGridView.setVisible(false);
            }

        }
    }

}

